# OpenEduCat
<hr>

## Admission Process

* Create Academic Year
````
  - Academic Year is the annual period of educational institution
    OpenEduCat ‣ Configuration ‣ Academic Year ‣ Create
````
* Create Academic Term
````
  - Academic Term is the subdivision of the Academic Year. It will be generated automatically based on the term structure we select for the academic year
    OpenEduCat ‣ Configuration ‣ Academic Year ‣ Create ‣ Create Terms

    * Another way to create_
      OpenEduCat ‣ Configuration ‣ Academic Term ‣ Create
````
* Create Fees Terms
````
    OpenEduCat ‣ Configuration ‣ Fees Terms ‣ Create
````
* Add Terms
````
  OpenEduCat ‣ Configuration ‣ Fees Terms ‣ Add Terms ‣ Save & Close
````
* Create Fees Elements
````
  OpenEduCat ‣ Configuration ‣ Fees Terms ‣ Add Terms ‣ Add Fees Elements ‣ Select/Create ‣ Save & Close

  * Another way to ceate_
    Invoicing ‣ Customers ‣ Products ‣ Create
````
* Create Course
````
  - Specify different facilities(Class1, Class2) of an institute
    OpenEduCat ‣ Configuration ‣ Course Management ‣ Courses ‣ Create
````
* Create Batch
````
  - Create batches under a particular course
    OpenEduCat ‣ Configuration ‣ Course Management ‣ Batches ‣ Create
````
* Create Subjects
````
  OpenEduCat ‣ Configuration ‣ Subject Management ‣ Subjects ‣ Create
````
* Add Subjects to Courses
````
  OpenEduCat ‣ Configuration ‣ Course Management ‣ Courses ‣ Add Subjects ‣ Select/Create ‣ Save
````
* Create Class Rooms
````
  - Class Room is created and alocated to a particular course
    OpenEduCat ‣ Configuration ‣ Class Room Management ‣ Class Rooms ‣ Create
````
* Create Facility
````
  - Specify different facilities(AC, TV, WiFi) of an institute
    OpenEduCat ‣ Configuration ‣ General Management ‣ Facilities ‣ Create
  ````
* Create Admission Register
````
  - Before the Enrollment, Admission Register is created for each course where admissions for that course falls in respective Admission Register.
    Admissions ‣ Admission Registers ‣ create

  - Workflow
    Draft ‣ Confirm ‣ Start Application
````
* Create Course Fees
````
  Admissions ‣ Admission Registers ‣ Course Fees ‣ create
  Invoicing ‣ Customers ‣ Products(Service) ‣ Create
````
* Enroll Student
````
  - Enrollment starts, Admission to particular student is done here
    Admissions ‣ Applications ‣ Create
  - Registration form contains_ Admission, Educational, Personal Details of a student are recorded

  - Workflow
    Draft ‣ Submit ‣ Confirm ‣ Enroll

  - To see student profile detail Click on Open Student Profile
````
* Alternative Way to Create Student Profile
````
  - Student module is developed to manage student detailed profile till the end of the academic session
    OpenEduCat ‣ Students ‣ Create

  - Student form contains_ Personal Information, Educational, Received Badges, Library, Fees Collection Detail, Achievements, Discipline, Activity Logs, Assignments, Parents, Health, Job Post, Placements, Skills, Alumni
````
* Create Single Student User At a time
````
  Student ‣ Profile ‣ Create Student User
  ````
* Create Multiple Student Users At a time
````
  Student ‣ Student’s List View ‣ Selects Students ‣ Action ‣ Create Users
  - To view the created Odoo users
    Settings ‣ Users & Companies ‣ Users
````

## Faculty Creation Process
* Create Faculty
````
  - Faculty profile is created and managed the faculty information here
    OpenEduCat ‣ Faculties ‣ Create

  - Faculty Form contains_ Personal Information, Subjects Detail, Health Detail, Library Detail, Sessions
  ````
* Create Faculty Employee/User
````
  - To log into the system as faculty, employee creation is needed
    OpenEduCat ‣ Faculties ‣ Faculty Member ‣ Create Employee/Create User
````

## Assignment Process
* Create Assignment Types
````
  - Create assignment types(Home Work, External Assignment)
    Assignments ‣ Configuration ‣ Assignment Types ‣ Create
````
* Create Assignments
````
  - Create multiple assignments under a particular assignment types
    Assignments ‣ Assignments ‣ Assignments ‣ Create ‣ Publish

  - Workflow
    Draft ‣ Publish ‣ Finish

  - Assignment form contains_
    ‣ Basic Information_ Issued Date, Submission Date, Marks, Questions
    ‣ Allocation Information_ Allocate the students   
    ‣ Submissions_ View the submitted assignments
````
* Assignment Submissions
````
  - List of all the assignments submitted by the students are displayed here
    Assignments ‣ Assignments ‣ Assignment Submissions ‣ Create ‣ Submit ‣ Accept/Change Require/Reject

  - Workflow
    Draft ‣ Submit ‣ Accept/Reject

  - Assignment Submissions form contains_
    ‣ Student_ Specify the student whose assignment is to be submitted
    ‣ Assignment_ Select the name of the assignment from the list of available assignments or create a new assignment
    ‣ Submission Date_ By default todays date and time is set in the Submission Date
    ‣ Description and Note_ Provide description and note about the assignment
````

## Attendance Process
* Create Attendance Register
````
  - To maintain attendance, attendance register is created for particular batch and course
    Attendances ‣ Attendance ‣ Registers ‣ Create
````
* Create Timings
````
  - To manage the timing details, timings are created
    Timetables ‣ Configuration ‣ Timings ‣ Create

  - Timing form contains_
    ‣ Name: Specify the name of the session in timetable(10AM- 11AM, 11AM- 12PM)
    ‣ Hour: Specify the starting hour of the session(10, 11)
    ‣ Minute: Specify the starting minute of the session(00)
    ‣ AM/PM: Specify the meridiem of the time(AM/PM)
    ‣ Duration: Specify the duration of the session(01:00)
    ‣ Sequence: Set Sequence of the session
````
* Create Session
````
  - Day wise time table are recorded
    TimeTables ‣ Sessions ‣ Create
````
* Create Multiple sessions
````
  - Generate weeks Schedule here
    Timetables ‣ Configuration ‣ Generate Sessions ‣ Proceed
````
* Create Attendance Sheet
````
  - Attendance sheet is created as par particular date and maintain attendance for that day
    Attendances ‣ Attendance ‣ Sheets ‣ Create
````
* Take Attendance
````
  Attendances ‣ Attendance ‣ Sheets ‣ Attendance Start ‣ Add Student ‣ Attendance Taken ‣ Save

  * Another way to Take Attendances
    Timetables ‣ Select Session ‣ Attendance Sheet ‣ Proceed
````

## Exam Process
* Create Exam Types
````
  - Create different Exam types(Sessional, Annual)
    Exams ‣ Configuration ‣ Exam Types ‣ Create
````
* Create Exam Rooms
````
  - Select rooms for exams
    Exams ‣ Configuration ‣ Exam Types ‣ Create
````
* Configure Grade
````
    Exams ‣ Configuration ‣ Grade Configuration ‣ Create
````
* Create Exam Sessions
````
  - Create exam session based on the course and batch
    Exams ‣ Exams ‣ Exam Sessions ‣ Create
````
* Create Exam Sessions
````
  - Create exam session based on the course and batch
    Exams ‣ Exams ‣ Exam Sessions ‣ Create

  - Create Venue(Room that will be used for exam) for the exam session
    Exams ‣ Exams ‣ Exam Sessions ‣ Venue ‣ Create

  - Workflow
    Draft ‣ Scheduled ‣ Held
````
* Create Exams
````
  - Exam Session contains multiple exams and each individual exam can be created with respect to each subject
    Exams ‣ Exams ‣ Exams ‣ Create

  - Workflow
    Draft ‣ Schedule Exam ‣ Select Exam Rooms ‣ Schedule Exam ‣ Held ‣ Select Absent Attendees ‣ Held
````
* Update Results
````
  Exams ‣ Exams ‣ Exam Attendees ‣ Select Attendee ‣ Update Marks ‣ Save
````
* Update Exams
````
  Exams ‣ Exams ‣ Exams ‣ Result Updated ‣ Done
````
* Update Exam Sessions
````
  Exams ‣ Exams ‣ Exam Sessions ‣ Done
````
* Create Result Template
````
  Exams ‣ Exams ‣ Result Templates ‣ Create ‣ Generate Result ‣ Save
````
* Validate Results
````
  Exams ‣ Exams ‣ Marksheet Registers ‣ Select Marksheet Register ‣ Validate Result
````
* View Results
````
  Exams ‣ Exams ‣ Marksheet Lines ‣ Select Student ‣ View/Edit Result

  - View particular exam result
    Exams ‣ Exams ‣ Result Lines ‣ Select Student ‣ View/Edit Result
````

## Library Process
* Create Media Type
````
  - The Media Type are used for specifying the sub-category of the media(Course Book, Reference Books, Magazine)
    Library ‣ Configuration ‣ Media Type ‣ Create
````
* Create Publishers
````
  Library ‣ Configuration ‣ Publishers ‣ Create
````
* Create Authors
````
  Library ‣ Configuration ‣ Authors ‣ Create
````
* Create Media
````
  - Create records of the medias to accommodate all the media details.
    Library ‣ All Media ‣ Media ‣ Create
````
* Create Media Units
````
  - For a particular media, multiple media unit is created for each available media present in the library.
    Library ‣ All Media ‣ Media Units ‣ Create
````
* Create Library Card Types
````
  - Library cards can be available in different types as it can be used by different users(students, faculties) and it may have different rules application.
    Library ‣ Configuration ‣ Library Card Types ‣ Create

  - Library Card Types form contains_
    ‣ Name: Name is the title of library card type.
    ‣ Number of medias Allowed: Specify the number of medias allowed on a card.
    ‣ Duration: Duration is the number of days for which the media is allowed to be kept with the person.
    ‣ Penalty Amount Per Day: You can specify the penalty given per day to the person, who returns the media after due date in Penalty Amount Per Day field.
````
* Create Library Cards
````
  - Create a library card to assign to a person
    Library ‣ Configuration ‣ Library Cards ‣ Create
````
* Media Movements
````
  - Whenever any media unit is issued or returned its movement is recorded in the media Movement.

  - Create media movement to issue book to a person
    Library ‣ All Media ‣ Media Movements ‣ Create

  - Workflow
    Available ‣ Issued ‣ Returned
````
* Media Queue Requests
````
  - A media queue request is created whenever a media is already allocated to someone else
    Library ‣ All Media ‣ Media Queue Requests ‣ Create
````
* Media Purchase Requests
````
  - A media purchase request is created whenever a media is not available in library
    Library ‣ All Media ‣ Media Purchase Requests ‣ Create
````
